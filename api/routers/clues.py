from routers.categories import CategoryOut
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

router = APIRouter()

class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool

class Clues(BaseModel):
    page_count: int
    clues: list[Clue]

class Message(BaseModel):
    message: str

@router.get("/api/clues", response_model=Clues)
def clues_list(page: int = 0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT clues.id,
                    clues.answer,
                    clues.question,
                    clues.value,
                    clues.invalid_count,
                    clues.canon,
                    categories.id,
                    categories.title,
                    categories.canon
                FROM clues
                JOIN categories
                    ON clues.category_id = categories.id
                LIMIT 100 OFFSET %s;
                """,
                [page * 100]
            )

            results = []
            for row in cur.fetchall():
                clue = {
                    "id": row[0],
                    "answer": row[1],
                    "question": row[2],
                    "value": row[3],
                    "invalid_count": row[4],
                    "category": {
                        "id": row[6],
                        "title": row[7],
                        "canon": row[8]
                    },
                    "canon": row[5]
                }
                results.append(clue)

            
            cur.execute(
                """
                SELECT COUNT(*) FROM clues;
            """
            )
            raw_count = cur.fetchone()[0]
            page_count = (raw_count // 100) + 1

            return Clues(page_count=page_count, clues=results)

@router.get(
    "/api/clues/{clue_id}",
    response_model=Clue,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT clues.id,
                    clues.answer,
                    clues.question,
                    clues.value,
                    clues.invalid_count,
                    clues.canon,
                    categories.id,
                    categories.title,
                    categories.canon
                FROM clues
                JOIN categories
                    ON clues.category_id = categories.id
                LIMIT 100 OFFSET %s;
            """,
                [clue_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Category not found"}
            return {
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category": {
                    "id": row[6],
                    "title": row[7],
                    "canon": row[8]
                },
                "canon": row[5]
            }

@router.delete(
    "/api/clues/{clue_id}",
    response_model=Message,
    responses={400: {"model": Message}},
)
def remove_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(
                    """
                    DELETE FROM clues
                    WHERE id = %s;
                """,
                    [clue_id],
                )
                return {
                    "message": "success",
                }
            except psycopg.errors.ForeignKeyViolation:
                response.status_code = status.HTTP_400_BAD_REQUEST
                return {
                    "message": "Cannot delete clue",
                }

@router.get(
    "/api/random-clue",
    response_model=Clue,
    responses={
        404: {
            "model": Message
        }
    }
)
def random_clue(boolean:bool=True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            if boolean == True:
                cur.execute(
                    f"""
                    SELECT clues.id,
                        clues.answer,
                        clues.question,
                        clues.value,
                        clues.invalid_count,
                        clues.canon,
                        categories.id,
                        categories.title,
                        categories.canon
                    FROM clues
                    JOIN categories
                        ON clues.category_id=categories.id
                    WHERE clues.invalid_count=0
                    ORDER BY random();
                    """
                )
            else:
                cur.execute(
                    f"""
                    SELECT clues.id,
                        clues.answer,
                        clues.question,
                        clues.value,
                        clues.invalid_count,
                        clues.canon,
                        categories.id,
                        categories.title,
                        categories.canon
                    FROM clues
                    JOIN categories
                        ON clues.category_id=categories.id
                    ORDER BY random();
                    """
                )
            row = cur.fetchone()

            return({
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category": {
                    "id": row[6],
                    "title": row[7],
                    "canon": row[8]
                },
                "canon": row[5]
            })